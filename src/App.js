import React from 'react';

import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import Header from './Components/Header/Header'
import SideBar from './Components/SideBar/SideBar'
import Game from './Components/Games/Games'
import TopStreams from './Components/TopStreams/TopStreams'
import Live from './Components/Live/Live'
import GameStreams from './Components/GameStreams/GameStreams'

import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Header/>
        <SideBar/>
        <Switch>
          <Route exact path='/' component={Game} />
          <Route  path='/top-streams' component={TopStreams} />
          <Route  path='/live/:slug' component={Live} />
          <Route path='/game/:slug' component={GameStreams} />
        </Switch>
      </div>
    </Router>
    
  );
}

export default App;
