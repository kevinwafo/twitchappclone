import React, {useState, useEffect} from 'react';

import api from '../../api'

import ReactTwitchEmbedVideo from 'react-twitch-embed-video'

import {useParams} from 'react-router-dom'

const Live = (props) => {

    let {slug} = useParams()

    const [infoStream, setInfoStream] = useState([])
    const [infoGame, setInfoGame] = useState([])

    useEffect(() => {

        const fetchData = async () => {

            const resultStream = await api.get(`https://api.twitch.tv/helix/streams?user_login=${slug}`)
            
            let gameId = `${resultStream.data.data[0].game_id}`
            const resultGame = await api.get(`https://api.twitch.tv/helix/games?id=${gameId}`)
            let nameGame = resultGame.data.data[0].name

            setInfoGame(nameGame)
            setInfoStream(resultStream.data.data[0])
            console.log(nameGame)
            
        }

        fetchData()

    },[slug])

    return (
        <div className='containerDecale'>
            <ReactTwitchEmbedVideo height='754' width='100%' channel={slug} />
            <div className="contInfo">
                <div className="titreStream">{infoStream.title}</div>
                <div className="viewer">Viewer : {infoStream.viewer_count}</div>
                <div className="infoGame">Streamer : {infoStream.user_name}, &nbsp; Langue : {infoStream.language} </div>
                <div className="nomJeu">Jeu : {infoGame}</div>
            </div>
        </div>
    );
};

export default Live;