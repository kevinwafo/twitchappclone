import React, {useState, useEffect} from 'react'
import api from '../../api'

const SideBar = () => {

    const [topStream, setTopStream] = useState([])

    useEffect(() => {
        const fetchData = async () => {

            const result = await api.get('https://api.twitch.tv/helix/streams')
            let dataArray = result.data.data

            //recuperation des id des games et des users
            let gameIds = dataArray.map(stream => {
                return stream.game_id
            })
            let userIds = dataArray.map(stream => {
                return stream.user_id
            })

            //creation des urls personnalisés
            let baseUrlGames = 'https://api.twitch.tv/helix/games?'
            let baseUrlUsers = 'https://api.twitch.tv/helix/users?'

            let querryParamsGames = ''
            let querryParamsUsers = ''

            gameIds.map(id => {
                return (querryParamsGames = querryParamsGames + `id=${id}&` )
            })
            userIds.map(id => {
                return (querryParamsUsers = querryParamsUsers + `id=${id}&` )
            })

            //url final
            let urlFinalGames =  baseUrlGames + querryParamsGames
            let urlFinalUsers = baseUrlUsers + querryParamsUsers
            
            //appel
            let gamesNames = await api.get(urlFinalGames)
            let usersNames = await api.get(urlFinalUsers)

            let gamesNamesArray = gamesNames.data.data
            let usersNamesArray = usersNames.data.data
            
            //creation du tableau final
            let finalArray = dataArray.map(stream => {
                //augmente 3 valeur
                stream.gameName = ''
                stream.truePic = ''
                stream.login = ''

                gamesNamesArray.forEach(game => {
                    usersNamesArray.forEach(user => {
                        if(stream.user_id === user.id && stream.game_id === game.id){
                            stream.gameName = game.name
                            stream.truePic = user.profile_image_url
                            stream.login = user.login
                        }
                    })
                })

                return stream
            })
            
            setTopStream(finalArray.slice(0,7))

        }

        fetchData()
        
    },[])
    
    return (
        <div className='sideBar'>
            <h2 className="titreSideBar">Chaine Recommandées</h2>
            <ul className="listeStream">
                {topStream.map((stream, index) => (
                    <li key={index} className="containerFlexSideBar">
                        <img src={stream.truePic} alt={stream.login} className="profilePicRonde"/>
                        <div className="streamUser">{stream.user_name}</div>
                        <div className="viewerRight">
                            <div className="pointRouge"></div>
                            <div>{stream.viewer_count}</div>
                        </div>
                        <div className="gameNameSideBar">{stream.gameName}</div>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default SideBar