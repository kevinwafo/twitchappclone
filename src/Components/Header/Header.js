import React, { Component } from 'react'

import {Link} from 'react-router-dom'

import logo from './IconTwitch.svg'
import search from './Search.svg'
import menuIco from './MenuIcon.svg'

class Header extends Component {
    render() {
        return (
            <div>

                <nav className="headerTop">
                    <ul className="listeMenu">
                        <li className="liensNav">
                            <Link className="lien" to='/'>
                                <img src={logo} alt="logoTwith" className="logo"/>
                            </Link>
                        </li>
                        <li className="liensNav">
                            <Link className="lien" to='/'>
                                Top Games 
                            </Link>
                        </li>
                        <li className="liensNav">
                            <Link className="lien" to='/top-streams'>
                                Top Streams
                            </Link> 
                        </li>
                        <li className="liensNav">
                            <form className="formSubmit">
                                <input type="text" className="inputRecherche"/>
                                <button type='submit'>
                                    <img src={search} alt="logo loupe" className="logoLoupe"/>
                                </button>
                            </form>
                        </li>
                    </ul>
                </nav>

                <div className="menuResBtn">
                    <img src={menuIco} alt="icone menu responsive" className="menuIco"/>
                </div>

            </div>
        )
    }
}

export default Header