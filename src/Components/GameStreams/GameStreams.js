import React, {useState, useEffect} from 'react';

import {useLocation, useParams, Link} from 'react-router-dom'

import api from '../../api'


const GameStreams = () => {
    let {slug} = useParams()
    let location = useLocation();

    const [streamData, setStreamData] = useState([]);
    const [viewer, setViewer] = useState(0);

    useEffect(() => {
        const fetchData = async () => {
            const result = await api.get(`https://api.twitch.tv/helix/streams?game_id=${location.state.gameID}`);
            
            let dataArray = result.data.data ;

            let finalArray = dataArray.map(stream => {
                let newUrl = stream.thumbnail_url
                .replace('{width}', '320')
                .replace('{height}', '180');
                stream.thumbnail_url = newUrl;
                return stream
            })

            //calcul les viewer
            let totalViewer = finalArray.reduce((acc, val) => {
                return acc + val.viewer_count;
            }, 0)

            let userIDs = dataArray.map(stream => {
                return stream.user_id
            });

            let baseUrl = 'https://api.twitch.tv/helix/users?';
            let queryParamsUsers = '';
            userIDs.map(id => {
                return queryParamsUsers = queryParamsUsers + `id=${id}&`;
            } );
            let finalUrl = baseUrl + queryParamsUsers;

            let getUsersLogin = await api.get(finalUrl);
            let usersLoginArray = getUsersLogin.data.data;

            finalArray = dataArray.map(stream => {
                stream.login = ''
                usersLoginArray.forEach(login => {
                    if(stream.user_id === login.id){
                        stream.login = login.login
                    }
                })
                return stream
            })

            setViewer(totalViewer);
            setStreamData(finalArray);
        }

        fetchData()
    },[location.state.gameID])
    
    return (
        <div>
            <h1 className="titreGameStreams">Stream : {slug}</h1>
            <h3 className="sousTitreGameStreams">
                <strong className="textColored">{viewer} </strong>
                personnes regardent {slug}
            </h3>
            <div className="flexAccueil">
                {streamData.map((stream, index) => (
                    <div key={index} className="carteGameStreams">
                        <img className="imgCartes" src={stream.thumbnail_url} alt={'image stream '+index} />
                        <div className="cardBodyGameStreams">
                            <h5 className="titreCarteStream">{stream.user_name}</h5>
                            <p className="txtStream">Nombre de viewer : {stream.viewer_count}</p>
                            <Link className='lien' to = {`/live/${stream.login}`} >
                                <div className="btnCarte">Regarder {stream.user_name}</div>
                            </Link>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default GameStreams;