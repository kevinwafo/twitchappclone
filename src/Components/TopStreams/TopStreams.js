import React, {useEffect, useState, Fragment} from 'react';


import { Link } from 'react-router-dom';

import api from '../../api'

const TopStreams = () => {

    const [topChannels, setTopChannels] = useState([])

    useEffect(() => {
        const fetchData = async () => {

            const result = await api.get('https://api.twitch.tv/helix/streams')
            let dataArray = result.data.data

            //recuperation des id des games et des users
            let gameIds = dataArray.map(stream => {
                return stream.game_id
            })
            let userIds = dataArray.map(stream => {
                return stream.user_id
            })

            //creation des urls personnalisés
            let baseUrlGames = 'https://api.twitch.tv/helix/games?'
            let baseUrlUsers = 'https://api.twitch.tv/helix/users?'

            let querryParamsGames = ''
            let querryParamsUsers = ''

            gameIds.map(id => {
                return (querryParamsGames = querryParamsGames + `id=${id}&` )
            })
            userIds.map(id => {
                return (querryParamsUsers = querryParamsUsers + `id=${id}&` )
            })

            //url final
            let urlFinalGames =  baseUrlGames + querryParamsGames
            let urlFinalUsers = baseUrlUsers + querryParamsUsers
            
            //appel
            let gamesNames = await api.get(urlFinalGames)
            let usersNames = await api.get(urlFinalUsers)

            let gamesNamesArray = gamesNames.data.data
            let usersNamesArray = usersNames.data.data
            
            //creation du tableau final
            let finalArray = dataArray.map(stream => {
                //augmente 3 valeur
                stream.gameName = ''
                stream.truePic = ''
                stream.login = ''

                gamesNamesArray.forEach(game => {
                    usersNamesArray.forEach(user => {
                        if(stream.user_id === user.id && stream.game_id === game.id){
                            stream.gameName = game.name
                            stream.login = user.login
                        }
                    })
                })
                let newUrl = stream.thumbnail_url
                             .replace('{width}', '320')
                             .replace('{height}', '180')
                stream.thumbnail_url = newUrl
                return stream
            })
            
            setTopChannels(finalArray)

        }

        fetchData()
        
    },[])

    return (
        <Fragment>
           <h1 className="titreGames">Streams les plus populaire</h1>
           <div className="flexAccueil">
                {topChannels.map((stream, index) => (
                    <div key={index} className="carteStream">
                        <img src={stream.thumbnail_url} className='imgCartes' alt={stream.gameName}/>
                        <div className="cardBodyStream">
                            <h5 className="titreCarteStream">{stream.user_name}</h5>
                            <p className="txtStream">Jeu : {stream.gameName} </p>
                            <p className="txtStream viewers">viewers : {stream.viewer_count}</p>
                            <div className="btnCarte">
                                <Link className='lien' to={`/live/${stream.login}`} > 
                                    Regarder {stream.user_name}
                                </Link>
                            </div>
                        </div>
                    </div>
                ))}
           </div>
        </Fragment>
    );
};

export default TopStreams;